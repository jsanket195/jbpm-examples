package org.jbpm.spring.boot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ApplicationHome {

  @RequestMapping("/")
  @ResponseBody
  public String index() {
    return "<h1>Home Page</h1>";
  }

}
