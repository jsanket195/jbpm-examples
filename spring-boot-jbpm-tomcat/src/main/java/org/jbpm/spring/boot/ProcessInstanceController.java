package org.jbpm.spring.boot;

import java.util.Collection;

import org.jbpm.services.api.ProcessService;
import org.jbpm.services.api.RuntimeDataService;
import org.jbpm.services.api.model.ProcessInstanceDesc;
import org.kie.internal.query.QueryContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class ProcessInstanceController {
	
	@Autowired
	private RuntimeDataService runtimeDataService;
	
	@Autowired
	private ProcessService processService;

	@RequestMapping(value = "/processinstance", method = RequestMethod.GET)
	public Collection<ProcessInstanceDesc> getProcessInstances() {
		
		Collection<ProcessInstanceDesc> processInstances = runtimeDataService.getProcessInstances(new QueryContext(0, 100, "status", true));

		return processInstances;
 
	}
	
	@RequestMapping(value = "/processinstance/show/{id}", method = RequestMethod.GET)
	public ProcessInstanceDesc getProcessInstance(@PathVariable String id) {
		long processInstanceId = Long.parseLong(id);
		ProcessInstanceDesc processInstance = runtimeDataService.getProcessInstanceById(processInstanceId);

		return processInstance;
	}
	
	@RequestMapping(value = "/processinstance/abort/{id}", method = RequestMethod.POST)
	public String abortProcessInstance(@PathVariable String id) {
		
		processService.abortProcessInstance(Long.parseLong(id));
		
		return "Instance (" + id + ") aborted successfully";
	}
	
	@RequestMapping(value = "/processinstance/signal/{id}", method = RequestMethod.POST)
	public String signalProcessInstance(@PathVariable String id, @RequestParam String signal, @RequestParam String data) {
		
		processService.signalProcessInstance(Long.parseLong(id), signal, data);

		return "Signal sent to instance (" + id + ") successfully";
	}
}
